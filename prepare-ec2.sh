#!/bin/bash
#
# Configuration script, to be run on top of an off-the-shelf 64-bit Amazon
# Linux AMI

# docker
sudo yum update -y
sudo yum install -y docker
sudo usermod -aG docker ec2-user
sudo service docker start

# mount the EFS file systems
sudo yum install -y nfs-utils

mkdir crawl
sudo mount -t nfs4 \
  -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 \
  fs-6eda7c27.efs.us-east-1.amazonaws.com:/ crawl
sudo chown -R $(whoami) crawl

mkdir coverage
sudo mount -t nfs4 \
  -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 \
  fs-e18024a8.efs.us-east-1.amazonaws.com:/ coverage
sudo chown -R $(whoami) coverage

mkdir results
sudo mount -t nfs4 \
  -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 \
  fs-e38024aa.efs.us-east-1.amazonaws.com:/ results
sudo chown -R $(whoami) results

mkdir cache
sudo mount -t nfs4 \
  -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 \
  fs-e88024a1.efs.us-east-1.amazonaws.com:/ cache
sudo chown -R $(whoami) cache
