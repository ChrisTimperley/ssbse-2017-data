#!/bin/bash
bug=$1
docker_image="christimperley/repairbox:${bug}"

# probably need to give execution permissions to the docker
# user (uid = 1000)
results=$(mktemp -d)
sudo chown -R 1000 "${results}"

# launch GenProg container
docker pull christimperley/genprog
docker create --log-driver none \
              -v /opt/genprog3 \
              --name genprog \
              christimperley/genprog

# launch bug container
docker pull "$docker_image" && \
docker run -i --rm \
      --env EXTRA_PATH="/opt/genprog3" \
      --env PYTHIA_PLATFORM_SCALE="2.5" \
      --env PYTHIA_CONSTANT_NOISE="1.0" \
      --env TIME_LIMIT="43200" \
      --log-driver none \
      --volumes-from genprog \
      --name "rbox-${bug}" \
      -v ${results}:/experiment/scratch \
      -v ${PWD}/controller:/experiment/controller:ro \
      --entrypoint /experiment/controller/crawl.sh \
      ${docker_image}

# housekeeping
docker volume prune -f

# repossess the contents of the scratch directory
sudo chown -R $(whoami) "${results}"

# copy the results to the shared crawl directory
mkdir -p "${PWD}/results"
mv "${results}" "${PWD}/results/${bug}"
