#!/bin/bash

# git config
git config --global user.email "christimperley@googlemail.com"
git config --global user.name "ChrisTimperley"

# install docker
# sudo apt-get install -y docker.io

# possible fix to:
# https://bugzilla.redhat.com/show_bug.cgi?id=1420147
# https://github.com/docker/adocker/issues/14504
# wget https://github.com/docker/docker/archive/v17.04.0-ce.tar.gz
# tar -xvf v17.04.0-ce.tar.gz

# create docker group
sudo gpasswd -a ${USER} docker

# update docker
update_engine_client -update

# start running experiment

# fire email on completion

# shutdown machine
# sudo shutdown 5
