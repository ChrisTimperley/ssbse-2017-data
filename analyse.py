#!/usr/bin/python3
import csv
import os
import json
import math
from pprint import pprint
from tabulate import tabulate as tbl

import numpy as np
import scipy
import scipy.stats
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from scipy.stats.kde import gaussian_kde
from numpy import linspace

# Computes Vargha-Delaney A12 measure of effect size
#
# Adapted from Susan Stepney's R implementation:
# - https://www.cs.york.ac.uk/nature/tangent/stats.pdf
def a12(a, b):
    (ranksum, p) = scipy.stats.mannwhitneyu(a, b, alternative='two-sided')
    n = len(a)
    m = len(b)
    a = (ranksum/n - (n + 1)/2)/m
    a = 1 - a if a < 0.5 else a
    return a


# Calculates the likelihood that a "fixed" statement will be chosen from the
# fault localisation
def likelihood_of_selection(localisation, fixed):
    sm = sum(localisation.values())
    if sm == 0:
        return 0.0
    localisation = {sid: (v / sm) for (sid, v) in localisation.items()}
    prob = sum(localisation.get(sid, 0.0) for sid in fixed)
    return prob

#def localisation_p2f_drop(scenario):
#    mutants = [m for m in scenario.mutants() if not m.acceptable() and m.compiled]
#    mutants_by_sid = Mutant.group_by_stmt(mutants)
#    mu = {}
#
#    buckets = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
#    members = []
#    no_cov = []
#
#    # count the number of items in each bucket
#    for sid in scenario.stmts():
#        if not sid in mutants_by_sid:
#            no_cov.append(sid)
#            continue
#
#        # allocate to a bucket
#        p2f = np.mean([m.p2f() for m in mutants_by_sid[sid]])
#        for i in len(buckets):
#            if p2f < buckets[i]:
#                members[i].append(sid)
#                break
#
#    # assign probabilities to each bucket
#    tally = []
#    for (i, bucket) in enumerate(buckets):
#        
#    
#        #p2f = np.mean([m.p2f() for m in mutants_by_sid[sid]])
#        #mu[sid] = 1.05 - (p2f ** 2)
#        
#    return mu

def localisation_metallaxis(scenario):
    mu = {}
    mutants = [m for m in scenario.mutants() if m.compiled and not m.acceptable()]
    mutants_by_sid = Mutant.group_by_stmt(mutants)
    for sid in scenario.stmts():
        if sid in mutants_by_sid:
            stmt_mutants = mutants_by_sid[sid]
            mu[sid] = max(m.suspiciousness() for m in stmt_mutants)
        else:
            mu[sid] = 1.0
    return mu

def localisation_muse(scenario):
    mu = {}
    mutants = [m for m in scenario.mutants() if m.compiled and not m.acceptable()]
    # remove neutrals
    mutants = [m for m in scenario.mutants() if m.neutral()]
    mutants_by_sid = Mutant.group_by_stmt(mutants)

    # determine f2p and p2f
    f2p = p2f = 0
    for m in mutants:
        f2p += m.num_f2p()
        p2f += m.num_p2f()

    for sid in scenario.stmts():
        if sid in mutants_by_sid:
            stmt_mutants = mutants_by_sid.get(sid, [])
            a = 1 / (len(stmt_mutants) + 1)
            b = 0
            for m in stmt_mutants:
                b += m.f2p() / (f2p + 1)
                b -= m.p2f() / (p2f + 1)
            mu[sid] = a * b
        else:
            mu[sid] = 0.0 # BE CAREFUL!


    # perform normalisation
    min_mu = min(mu)
    max_mu = max(mu)
    mu = {sid: ((mu_sid - min_mu) / (max_mu - min_mu)) for (sid, mu_sid) in mu.items()}

    return mu

def localisation_spectrum(scenario, susp):
    mu = {}
    mutants_by_sid = Mutant.group_by_stmt([m for m in scenario.mutants() if m.compiled])
    for sid in scenario.stmts():
        # find the number of positive and negative tests executed at this statement
        if sid in mutants_by_sid:
            muts = mutants_by_sid[sid]
            ep = len(muts[0].positives())
            np = scenario.num_passing_tests() - ep
            ef = len(muts[0].negatives())
            nf = scenario.num_failing_tests() - ef
            mu[sid] = susp(ep, np, ef, nf)

        else:
            mu[sid] = 0.1
    return mu

def localisation_genprog(scenario):
    def susp(ep, np, ef, nf):
        if ef > 0 and ep == 0:
            return 1.0
        elif ef > 0:
            return 0.1
        else:
            return 0
    return localisation_spectrum(scenario, susp)

def localisation_jaccard(scenario):
    def susp(ep, np, ef, nf):
        return ef / (ef + ep)
    return localisation_spectrum(scenario, susp)

def localisation_ochiai(scenario):
    def susp(ep, np, ef, nf):
        return ef / math.sqrt((ef + nf) * (ef + ep))
    return localisation_spectrum(scenario, susp)

def localisation_tarantula(scenario):
    def susp(ep, np, ef, nf):
        a = ef / (ef + nf)
        b = ep / (ep + np)
        b = a + b
        return a / b
    return localisation_spectrum(scenario, susp)

# calculates the stable P2F for a given scenario
def localisation_stable_p2f(scenario):
    mutants = [m for m in scenario.mutants() if not m.acceptable() and m.compiled]
    mutants_by_sid = Mutant.group_by_stmt(mutants)
    mu = {}
    for sid in scenario.stmts():
        if not sid in mutants_by_sid:
            stmt_mutants = []
        else:
            stmt_mutants = mutants_by_sid[sid]

        num_stmt_mutants = len(stmt_mutants)
        p2fs = [1 - m.p2f() for m in stmt_mutants]
        p2fs.append(0.5)
        p2fs = sum(p2fs)

        left = 1 / (num_stmt_mutants + 1)
        score = left * p2fs
        
        mu[sid] = score
    return mu

# calculates the adjusted coverage localisation for a given scenario
def localisation_adjusted_coverage(scenario):
    adjusted_coverage = scenario.adjusted_coverage()
    num_low = sum(1 for c in adjusted_coverage.values() if c <= 0.02)
    num_high = len(scenario.stmts()) - num_low
    
    # high weight
    if num_high > 0:
        weight_high = 0.1 / num_high
    else:
        weight_high = None

    # low weight
    if num_low > 0:
        weight_low = 0.9 / num_low
    else:
        weight_low = None

    loc = {}
    for sid in scenario.stmts():
        # if we don't know about the number of executed tests, assume high weight
        if sid in adjusted_coverage:
            c = adjusted_coverage[sid]
            loc[sid] = weight_low if c <= 0.02 else weight_high
        else:
            loc[sid] = weight_high
    return loc

def localisation_p2f_cov(scenario):
    cov = localisation_adjusted_coverage(scenario)
    p2f = localisation_stable_p2f(scenario)
    mu = {}
    for sid in cov:
        mu[sid] = cov[sid] * p2f[sid]
    return mu



class Mutant(object):

    # Groups mutants together by their SIDs
    @staticmethod
    def group_by_stmt(mutants):
        grouped = {}
        for m in mutants:
            sid = m.sid()
            if not sid in grouped:
                grouped[sid] = []
            grouped[sid].append(m)
        return grouped

    def __init__(self, results):
        self.edit = results['genome']
        self.results = results['results']
        self.compiled = results['compilation_success']
    def descriptor(self):
        return self.edit
    def kind(self):
        return self.descriptor()[0]
    def sid(self):
        return int(self.edit.split(' ')[1])

    # is this an acceptable repair?
    def acceptable(self):
        return self.compiled and all(self.results.values())

    def positives(self):
        return {t: r for (t, r) in self.results.items() if t[0] == 'p'}
    def negatives(self):
        return {t: r for (t, r) in self.results.items() if t[0] == 'n'}

    def positive_results(self):
        return [r for (n, r) in self.results.items() if n[0] == 'p']
    def negative_results(self):
        return [r for (n, r) in self.results.items() if n[0] == 'n']

    # checks whether this mutant covers any positive test cases
    def covers_positives(self):
        return self.positive_results() != []

    # is this a strictly neutral change?
    def neutral(self):
        return self.passes_positives() and self.fails_all_negatives()

    def positives_failed(self):
        return sum(1 for res in self.positive_results() if not res)
    def negatives_passed(self):
        return sum(1 for res in self.negative_results() if res)
 
    def lethal(self):
        return self.fails_all_positives() and self.fails_all_negatives()
    def passes_positives(self):
        res = self.positive_results()
        return self.compiled and (res == [] or all(res))
    def passes_negatives(self):
        res = self.negative_results()
        return self.compiled and (res == [] or all(res))

    def fails_all_positives(self):
        res = self.positive_results()
        return self.compiled and res != [] and not any(res)
    def fails_all_negatives(self):
        res = self.negative_results()
        return self.compiled and not any(res)

    def fails_any_positive(self):
        res = self.positive_results()
        return self.compiled and res != [] and not all(res)
    def passes_any_negative(self):
        res = self.negative_results()
        return self.compiled and res != [] and any(res)

    # find the number of test cases that kill this mutant
    # (i.e. the number of test cases failed by the mutant)
    def num_killed(self):
        return sum(1 for r in self.results.values() if not r)

    # find the number of positive test cases that kill this mutant
    # (i.e. the number of positive test cases failed by the mutant)
    def num_killed_positive(self):
        return sum(1 for r in self.positive_results() if not r)

    # find the number of negative test cases that kill this mutant
    # (i.e. the number of negative test cases failed by the mutant)
    def num_killed_negative(self):
        return sum(1 for r in self.negative_results() if not r)

    # computes the suspiciousness of this mutant, according to
    # Metallaxis's suspiciousness metric
    def suspiciousness(self):
        nk = self.num_killed()
        if nk > 0:
            return self.num_killed_negative() / nk
        else:
            return 1.0

    # fraction of positive tests that fail
    def p2f(self):
        pos = self.positive_results()
        num = len(pos)
        if num == 0:
            return 0.0
        else:
            return sum(1 for r in pos if not r) / num
            #return len([x for x in pos if x]) / num

    # fraction of negative tests that pass
    def f2p(self):
        neg = self.negative_results()
        num = len(neg)
        if num == 0:
            return 0.0
        else:
            return sum(1 for x in neg if x) / num

    def num_f2p(self):
        return sum(self.negative_results())

    def num_p2f(self):
        return sum(not r for r in self.positive_results())


# holds the mutation analysis for a given scenario
class Scenario(object):

    def __init__(self, name, directory):
        self.__name = name

        self.__human_fix_locations = set()
        location_file = os.path.join(directory, "locations.txt")
        with open(location_file, "r") as f:
            for location in f:
                location = int(location.strip())
                self.__human_fix_locations.add(location)

        # lines of code
        loc_file = os.path.join(directory, "loc.txt")
        with open(loc_file, "r") as f:
            self.__loc = int(f.read().strip())

        # load details
        problem_file = os.path.join(directory, "{}.problem.json".format(name)) 
        with open(problem_file, "r") as f:
            details = json.load(f)
            details = details['problem']
            self.__positive_tests = details['positive_tests']
            self.__negative_tests = details['negative_tests']

        # load mutants
        crawl_file = os.path.join(directory, "{}.walk.json".format(name))
        with open(crawl_file, "r") as f:
            self.__mutants = json.load(f)
        self.__mutants = {name: Mutant(m) for (name, m) in self.__mutants.items()}

        # set of stmts
        self.__stmts = set(m.sid() for m in self.__mutants.values())

        # compute number of statements in fault localisation
        stmts_file = os.path.join(directory, "stmts.txt")
        if os.path.exists(stmts_file):
            with open(stmts_file, "r") as f:
                self.__num_stmts = int(f.read().strip())
        else:
            self.__num_stmts = len(set(m.sid() for m in self.__mutants.values()))

    def name(self):
        return self.__name
    def num_stmts(self):
        return self.__num_stmts
    def num_tests(self):
        return self.__positive_tests + self.__negative_tests
    def num_passing_tests(self):
        return self.__positive_tests
    def num_failing_tests(self):
        return self.__negative_tests
    def stmts(self):
        return self.__stmts
    def kloc(self):
        return round(self.__loc / 1000)
    def has_solution(self):
        return any(True for m in self.mutants() if m.acceptable())

    def human_fix_locations(self):
        return self.__human_fix_locations
    def search_fix_locations(self):
        sols = [m for m in self.mutants() if m.acceptable()]
        return set(m.sid() for m in sols)

    def coverage(self):
        cov = {}
        grouped = Mutant.group_by_stmt([m for m in self.mutants() if m.compiled])
        for (sid, mutants) in grouped.items():
            num_pos = len(mutants[0].positive_results())
            cov[sid] = num_pos
        return cov

    def adjusted_coverage(self):
        cov = self.coverage()
        min_cov = min(cov.values())
        pos_tests = self.__positive_tests
        for (sid, c) in cov.items():
            if pos_tests == 0:
                c = 0.0
            else:
                c = (c - min_cov) / pos_tests
            cov[sid] = c
        return cov

    # returns a list of all mutants for this scenario
    def mutants(self):
        return self.__mutants.values()

# summary
def summary(scenarios):
    info = {}
    for scenario in scenarios:
        mutants = scenario.mutants()
        num_mutants = len(mutants)

        # percentage of lethal mutants
        lethal = sum(1 for m in mutants if m.lethal())
        lethal = (lethal / num_mutants) * 100.0
        lethal = '{0:.2f}'.format(lethal)

        # percentage of neutral mutants
        neutral = sum(1 for m in mutants if m.neutral())
        neutral = (neutral / num_mutants) * 100.0
        neutral = '{0:.2f}'.format(neutral)

        # percentage of compiling mutants
        compiling = sum(1 for m in mutants if m.compiled)
        compiling = (compiling / num_mutants) * 100.0
        compiling = '{0:.2f}'.format(compiling)

        # number of fixes found
        fixes = [m for m in mutants if m.acceptable()]
        num_fixes = len(fixes)
        fixed_stmts = set(f.sid() for f in fixes)
        num_fixed_stmts = len(fixed_stmts)

        # number of overlapping fixed statements
        fixed_stmts_overlap = scenario.human_fix_locations().intersection(fixed_stmts)
        num_fixed_stmts_overlap = len(fixed_stmts_overlap)

        # number of statements
        num_stmts = scenario.num_stmts()

        # sample rate
        sample_rate = num_mutants / num_stmts
        sample_rate = '{0:.2f}'.format(sample_rate)

        # adjusted sample rate (remove non-compiling)
        sample_rate_adj = sum(1 for m in mutants if m.compiled)
        sample_rate_adj = sample_rate_adj / num_stmts
        sample_rate_adj = '{0:.2f}'.format(sample_rate_adj)

        info[scenario.name()] = {
            'Stmts': num_stmts,
            'Mutants': num_mutants,
            'Lethal': lethal,
            'Neutral': neutral,
            'Compiling': compiling,
            'Fixes': num_fixes,
            'Fixed Stmts': num_fixed_stmts,
            'Fixed Stmt Overlap': num_fixed_stmts_overlap,
            'Sample Rate': sample_rate,
            'Sample Rate Adj': sample_rate_adj
        }
   
    # convert to a table and print
    return tabulate(info, [ 'Scenario',
                            'Stmts',
                            'Mutants',
                            'Lethal',
                            'Neutral',
                            'Compiling',
                            'Fixes',
                            'Fixed Stmts',
                            'Fixed Stmt Overlap',
                            'Sample Rate',
                            'Sample Rate Adj'])

# produces a description of each bug scenario
def description(scenarios):
    desc = {}
    for scenario in scenarios:
        desc[scenario.name()] = {
            'Stmts': scenario.num_stmts(),
            'Tests': scenario.num_tests(),
            'KLOC': scenario.kloc()
        }
    return tabulate(desc, ['Scenario', 'Stmts', 'Tests', 'KLOC'])

# compares the (average) P2F distribution for "fixed" and "faulty" statements
def rq_p2f(scenarios):
    fixed_p2fs = []
    faulty_p2fs = []
    for scenario in scenarios:
        fixed_stmts = scenario.human_fix_locations()
        scenario_muts = [m for m in scenario.mutants() if m.compiled and not m.acceptable()]
        for (sid, stmt_muts) in Mutant.group_by_stmt(scenario_muts).items():
            p2f = np.mean([m.p2f() for m in stmt_muts])
            if sid in fixed_stmts:
                fixed_p2fs.append(p2f)
            else:
                faulty_p2fs.append(p2f)

    # plot
    fig = plt.figure(1, figsize=(4.5, 3.0))
    ax = fig.add_subplot(111)
    bp = ax.boxplot([fixed_p2fs, faulty_p2fs])
    ax.set_ylim([-0.005, 1.000])
    ax.set_title('Mean $p2f$ of all mutants at a given statement')
    ax.set_ylabel('$p2f$')
    ax.set_xticklabels(['Faulty', 'Correct'])
    fig.savefig('mean_p2f.pdf', bbox_inches='tight')
    #plt.show()


    # effect size
    print("A (P2F): {}".format(a12(fixed_p2fs, faulty_p2fs)))
    print("Mean(P2F) - KS: {}".format(scipy.stats.ks_2samp(fixed_p2fs, faulty_p2fs)))

# compares the (average) F2P distribution for "fixed" and "faulty" statements
def rq_f2p(scenarios):
    fixed_f2ps = []
    faulty_f2ps = []
    for scenario in scenarios:
        fixed_stmts = scenario.human_fix_locations()
        scenario_muts = [m for m in scenario.mutants() if m.compiled and not m.acceptable()]
        for (sid, stmt_muts) in Mutant.group_by_stmt(scenario_muts).items():
            f2p = np.mean([m.f2p() for m in stmt_muts])
            if sid in fixed_stmts:
                fixed_f2ps.append(f2p)
            else:
                faulty_f2ps.append(f2p)

    # plot
#    fig = plt.figure(1, figsize=(4.5, 3.0))
#    ax = fig.add_subplot(111)
#    bp = ax.boxplot([fixed_f2ps, faulty_f2ps])
#    ax.set_ylim([-0.005, 1.000])
#    ax.set_title('Mean $f2p$ of all mutants at a given statement')
#    ax.set_ylabel('$f2p$')
#    ax.set_xticklabels(['Faulty', 'Correct'])
#    fig.savefig('mean_f2p.pdf', bbox_inches='tight')

    fig = plt.figure(1, figsize=(4.5, 3.0))
    ax = fig.add_subplot(111)
    bp = ax.boxplot([fixed_f2ps, faulty_f2ps], showfliers=False, sym='')
    ax.set_ylim([-0.010, 1.000])
    ax.set_title('(without outliers)')
    ax.set_ylabel('$f2p$')
    ax.set_xticklabels(['Faulty', 'Correct'])
    fig.savefig('mean_f2p_no_outliers.pdf', bbox_inches='tight')
 
    #plt.show()

    print("A (F2P): {}".format(a12(fixed_f2ps, faulty_f2ps)))
    print("Mean(F2P) - KS: {}".format(scipy.stats.ks_2samp(fixed_f2ps, faulty_f2ps)))

# performs an evaluation of several FL approaches, using the fixes found during
# the repair as the ground truth
def evaluation(scenarios):
    outcomes = {}
    scenarios = [s for s in scenarios if s.has_solution()]
    metrics = {
        'P2F': (lambda s: localisation_stable_p2f(s)),
        'Adjusted Coverage': (lambda s: localisation_adjusted_coverage(s)),
        'GP': (lambda s: localisation_genprog(s)),
        'P2F-Cov': (lambda s: localisation_p2f_cov(s)),
        'Metallaxis': (lambda s: localisation_metallaxis(s)),
        'MUSE': (lambda s: localisation_muse(s)),
        'Jaccard': (lambda s: localisation_jaccard(s)),
        'Ochiai': (lambda s: localisation_ochiai(s)),
        'Tarantula': (lambda s: localisation_tarantula(s))
    }
    for scenario in scenarios:
        name = scenario.name()
        print(name)
        outcomes[name] = {}
        #ground_truth = scenario.human_fix_locations()
        ground_truth = scenario.search_fix_locations()
        for (mname, metric) in metrics.items():
            p = likelihood_of_selection(metric(scenario), ground_truth)
            outcomes[name][mname] = '{0:.3f}'.format(p * 100.0)
    return tabulate(outcomes, ['Scenario',
                               'GP',
                               'Adjusted Coverage',
                               'P2F',
                               'P2F-Cov',
                               'Metallaxis',
                               'MUSE',
                               'Jaccard',
                               'Ochiai',
                               'Tarantula'])

# loads all scenario data from a given directory
def load(d):
    scenarios = [Scenario(name, os.path.join(d, name)) for name in os.listdir(d) \
                    if os.path.isdir(os.path.join(d, name))]
    return scenarios

def flatten(data, columns):
    return tbl(tabulate(data, columns))

def tabulate(data, columns):
    rows = [[c for c in columns]]
    key = columns.pop(0)
    for key in sorted(data.keys()):
        entry = data[key]
        row = [key] + [entry[c] for c in columns]
        rows.append(row)
    return rows

def to_csv(tbl, fn):
    with open(fn, 'w') as f:
        writer = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
        writer.writerows(tbl)
    print("Saved table to: {}".format(fn))

if __name__ == "__main__":
    results_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "results")
    scenarios = load(results_dir)
    #print(tbl(evaluation(scenarios)))
    fl_eval = evaluation(scenarios)
    print(tbl(fl_eval))
    to_csv(fl_eval, "cool.csv")
    #summary = summary(scenarios)
    #print(tbl(summary))
    #to_csv(summary, "summary.csv")
    #description = description(scenarios)
    #print(tbl(description))
    #to_csv(description, "description.csv")
    #rq_p2f(scenarios)
    rq_f2p(scenarios)
