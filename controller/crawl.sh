#!/bin/bash
PATH="${EXTRA_PATH}:${PATH}"
cd /experiment

# rebuild the program
(cd source && make)

genprog problem.json --mode walk --seed 0 --time-limit ${TIME_LIMIT}

# copy the results of the crawl to the shared scratch directory
cp ./*.walk.json scratch
